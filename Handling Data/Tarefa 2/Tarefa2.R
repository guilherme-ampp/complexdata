# Nome: Felipe Henriques da Silva
# Nome: Guilherme de Almeida

# A função getPrimeFactors recebe um número X e retorna uma matriz
# formada por duas linhas:
#   Na primera linha:
#     Os números primos que representam a fatoração de X. 
#   Na segunda linha:
#     A quantidade de vezes que o valor na mesma posição na primeira 
#     linha aparece na fatoração.
getPrimeFactors <- function(input) {
  # primeiro número primo
  prime <- 2
  # vetor para os números primos da fatoração
  result <- c()
  # vetor para os contadores dos números repetidos
  # durante a fatoração
  counter <- c()
  
  while(input > 1) {
    l <- length(result)
    
    if(input %% prime == 0) {
      input <- input / prime
      if(l == 0) {
        # se l é 0, começamos com os valores iniciais
        result <- prime
        counter <- 1
      } else {
        if(result[l] == prime) {
          # Se prime já aparece na posição atual de result
          # apenas somamos seu contador no vetor counter na
          # mesma posição
          counter[l] <- counter[l] + 1
        } else {
          # Caso contrário, andamos uma posição, guardamos
          # o número primo e começamos um novo contador na
          # nova posição
          l <- l+1
          result[l] <- prime  
          counter[l] <- 1
        }
      }
    } else {
      # se o input corrente não é divisível pelo primo atual
      # vamos para o próximo número
      prime <- prime + 1
    }
  }
  # retorna uma matriz formada pelos vetores result e counter
  # cada um como uma nova linha na matriz.
  return(rbind(result, counter))
}

# A função getNumberFromPrimeFactors recebe uma matriz
# cuja uma matriz:
#  Na primera linha:
#    Contém os números primos que representam a fatoração de um número. 
#  Na segunda linha:
#    Contém a quantidade de vezes que o valor na mesma posição na primeira
#    linha aparece na fatoração.
getNumberFromPrimeFactors <- function(m) {
  # comprimento da dimensão horizontal da matriz
  l <- length(m[1,])
  # contador da posição atual
  i <- 1
  total <- 1
  while(i <= l) {
    # para cada posição da primeira linha elevamos ao elemento na
    # mesma posição na segunda linha
    total <- total * m[1,i] ^ m[2,i]
    # incrementamos a posição
    i <- i + 1
  }
  # retorna a multiplicação da exponenciação dos elementos das colunas
  return(total)
}